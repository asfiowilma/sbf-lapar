function bgcol(){
    if(document.getElementById("main").style.backgroundColor == "black"){
        document.getElementById("main").style.backgroundColor = "white"
        document.getElementById("welcome").style.color = "black"
        document.getElementById("logo").src = "images/logodark.png"

    }
    else{
        document.getElementById("main").style.backgroundColor = "black"
        document.getElementById("welcome").style.color = "white"
        document.getElementById("logo").src = "images/logo.png"
    }
}